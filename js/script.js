window.onload=function(){

    let tbod=document.getElementById('tbod'),
        add=document.getElementById('add'),
        search=document.getElementById('search'),
        sel=document.getElementById('sel'),
        size=5,
        sorhId=document.getElementById('sortId'),
        sortFirst=document.getElementById('sortFirst'),
        temp=0;



    sel.addEventListener('change',function (e) {
        console.log(e.target.value)
        size=e.target.value;
        datatable()
    });

    let obj=[
        {
            id:2,
            firstname:'Narek',
            lastname:'Harutyunyan',
            number:4564646,
            age:22,
            show:true,


        },
        {
            id:1,
            firstname:'Andranik',
            lastname:'Grigoryan',
            number:252545,
            age:47,
            show:true,

        },
        {
            id:3,
            firstname:'Babken',
            lastname:'Suqiasya',
            number:25254,
            age:45,
            show:true,
        },
        {
            id:4,
            firstname:'Harut',
            lastname:'Ghazaryan',
            number:25254,
            age:45,
            show:true,
        },
        {
            id:6,
            firstname:'Shushan',
            lastname:'Poskalyan',
            number:25254,
            age:45,
            show:true,
        },

        {
            id:5,
            firstname:'Varazdat',
            lastname:'Mamikonyan',
            number:25254,
            age:45,
            show:true,
        },

        {
            id:7,
            firstname:'Frunz',
            lastname:'Hambardzumyan',
            number:25254,
            age:45,
            show:true,
        },

        {
            id:9,
            firstname:'Armen',
            lastname:'Tumanyan',
            number:25254,
            age:45,
            show:true,
        },

        {
            id:8,
            firstname:'Babken',
            lastname:'Grigoryan',
            number:25254,
            age:45,
            show:true,
        },

        {
            id:10,
            firstname:'Mane',
            lastname:'Grigoryan',
            number:25254,
            age:45,
            show:true,
        },

        {
            id:11,
            firstname:'Davo',
            lastname:'Grigoryan',
            number:25254,
            age:45,
            show:true,
        },
        {
            id:12,
            firstname:'Suro',
            lastname:'Grigoryan',
            number:25254,
            age:45,
            show:true,
        },

    ];


    obj=obj.sort((a, b) => parseFloat(a.id) - parseFloat(b.id));



    for (let i = 1; i <=(obj.length/4)+1 ; i++) {
        let paginate=document.getElementById('paginate');
             paginate.innerHTML+=`<li class="page-item"  ><a class="page-link page"  id="page" href="#">${i}</a></li>
`
    }

    let arrpage=document.getElementsByClassName('page');

    for (let i = 0; i <arrpage.length ; i++) {

        arrpage[i].addEventListener('click',function () {
            console.log(obj);
            paginate();
            let click=this.innerText;

            for (let j = 0; j <obj.length ; j++) {
                console.log(obj[j].page);


                if(obj[j].page==click){
                    console.log(click)
                    obj[j].show=true
                }else{
                    obj[j].show=false
                }
            }
            datatable()
            console.log(obj)
        })
    }



    function paginate(){
        let l=obj.length/4;
        let g=1;
        let b=1;
        for (let i = 0; i <obj.length ; i++) {

            if(g<=l){
                obj[i].page=b;
                ++g;
            }else{
                ++b;
                obj[i].page=b;
                g=2;
            }
        }
    }





    sorhId.addEventListener('click',function () {
        if(temp % 2 ===0){
            obj=obj.sort((a, b) => parseFloat(b.id) - parseFloat(a.id));
            temp++;
            console.log(this)
            this.innerHTML=`<i class="fa fa-arrow-up" style="color: blue" aria-hidden="true"> Id</i> `;
            return datatable()
        }else{
            obj=obj.sort((a, b) => parseFloat(a.id) - parseFloat(b.id));
            this.innerHTML=`<i class="fa fa-arrow-down" style="color: blue" aria-hidden="true"> Id</i>`;

            temp++;
            return datatable()
        }

    });
    sortFirst.addEventListener('click',function () {


        if(temp % 2 ===0){
            obj=obj.sort((a,b) => (a.firstname > b.firstname) ? 1 : ((b.firstname > a.firstname) ? -1 : 0));
            this.innerHTML=`<i class="fa fa-arrow-up" style="color: blue" aria-hidden="true"> First</i> `;

            temp++;
            return datatable()
        }else{
            obj=obj.sort((a, b) => parseFloat(a.id) - parseFloat(b.id));
            this.innerHTML=`<i class="fa fa-arrow-down" style="color: blue" aria-hidden="true"> First</i>`;

            temp++;
            return datatable()
        }

    });



    datatable();
    search.addEventListener('input', function (e) {
        console.log(e.target.value);

        let a=e.target.value
        if(a==''){
            for (let i = 0; i <obj.length ; i++) {
                obj[i].show=true
            }

            datatable()
        }
        for (let i = 0; i <obj.length ; i++) {
            let tru=obj[i].firstname.includes(a);
            if(tru==false){
                obj[i].show=false;
            }
        }
        console.log(obj)
        datatable()
    });
    add.addEventListener('click',function () {
       let name=document.getElementById('name').value,
           last=document.getElementById('last').value,
           number=document.getElementById('number').value,
           age=document.getElementById('age').value;
       if(name.length != 0 && last.length!=0 && number.length!=0 && age.length!=0){
           let lastId=obj[obj.length-1].id;
           let data={
                id:lastId+1,
                firstname:name,
                lastname:last,
                number:number,
                age:age,
               show:true
           };
           document.getElementById('name').value='';
           document.getElementById('last').value='';
           document.getElementById('number').value='';
           document.getElementById('age').value='';
           obj.push(data);
           tbod.innerHTML="";
           datatable()
       }else{
           alert("Required Inputs")
       }
    });

    function datatable(){
        tbod.innerHTML='';
        let t=size;
        obj.map((el)=>{

            if(el.show==true) {
                if(t>0){
                    tbod.innerHTML+=`
                 <tr>
                      <td >${el.id}</td>
                      <td >${el.firstname}</td>
                      <td>${el.lastname}</td>
                      <td>${el.number}</td>
                      <td>${el.age}</td>
                 </tr>
            `;
                    t--
                }

            }

        })

    }
    

};

